//Теоретичні питання

//1.Опишіть своїми словами як працює метод forEach.
//Метод forEach() виконує зазначену функцію один раз для кожного елемента в масиві.

//2.Як очистити масив?
//Масив можна очистити за допомогою надання нового значення для його ідентифікатора прив'язки (BindingIdentifier).
//По суті це не очищення як таке, а просто перепризначення "об'єкта масиву" для ідентифікатора прив'язки.

//3.Як можна перевірити, що та чи інша змінна є масивом?
//Глобальний об'єкт JavaScript Array - це конструктор для масивів, які на високому рівні є спископодібними об'єктами.

//Завдання

function filterBy(arr, type) {
    return arr.filter(element => checkType(element) !== type)
}

const arr = [1, {name: "user",}, true, null, undefined, "user", Symbol('$'), 1n]
const checkedTypes = ["string", "number", "object", "boolean", "null", "undefined", "symbol", "bigint"]
checkedTypes.forEach(type => {
    console.log(`${type} type deleted`)
    console.table(filterBy(arr, type))
})
